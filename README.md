# Description

The server for the Photonic event photo sharing service.

Unless otherwise specified, communication is done with JSON over HTTP.

The entire application could be modelled as MVC where the DB is the model, the API is the controller, and the client is the view/controller.
# API Specification

API endpoints
```
/login
	Input:
		User credentials
	Output:
		Unique session identifier
	Server logic:
		Maintain list of valid sessions in DB. 

/search
	Input:
		Session
		Event name search string
	Output:
		list of api urls for found events whose names (partially) match search string

/event/<event id>
	Input:
		Session
		Event identifier
	Output:
		Attributes of the desired events: name, date, location, associated images etc.
		The list of image ids/urls is the semantically important bit.
	Server logic:
		Check user permissions regarding event, event may be private, etc.

/event/image/<image id>
	Input:
		Session
		Image identifier
	Output:
		Serve image file
	Server logic:
		Ensure image filename of served file has semantic meaning.

/join/<event id>
	Input:
		Session
		Event identifier
	Output:
		Success/fail status of user associating with an event
	Server logic:
		Record user's association with target event
	
/leave/<event id>
	Input:
		Session
		Event identifier
	Output:
		Success/fail status of user associating with an event
	Server logic:
		Record user's association with target event

/upload
	Input:
		Session
		Event Identifier
		Image
	Output:
		Success/fail status
		Image id
	Server logic:
		Receive and store image file in cdn. Associate it with event.

```
# Server side spec

## Data Models

User

- username
- password
- real name
- profile photo

Event

- name
- start/end times
- location
- banner photo
- content photos
- access permissions

Photo

- physical file

## DB Models

User

- *PK* id
- username
- password
- real name

Photo

- *PK* id
- date uploaded
- physical path
- name

UserPhoto (association)

- *PK* (User, Photo)

Event

- *PK* id
- *FK* owner
- *FK* banner photo
- name
- start time
- end time
- location
- access permissions

EventPhoto (association)

- *PK* (Event, Photo)

EventAccess (association)

- *PK* id
- *FK* (Event, User)
- permission type (read/write/delete)


